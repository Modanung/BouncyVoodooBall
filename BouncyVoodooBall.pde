float x = 0;
float y = 0;
float z = 235.0;

float dx = 5;
float dy = 4;
float dz = 2;

float maxZ = 512.0;
float normZ = z / maxZ;

void setup()
{
  fullScreen(P2D, 0);
  x = random(width / 4, 3 * width / 4);
  y = random(height / 4, 3 * height / 4);
  fill(0,128, 0);
  noCursor();
  background(0);
}

void draw()
{
  x += dx + dx * pow(sin((millis() + z * 5) * 0.001), 2.0);
  y += dy + dy * pow(cos((millis() - z * 2.3) * 0.0011), 4.0);
  z += dz;
  normZ = z / maxZ;
  float invNormZ = 1 - normZ;
  
  blendMode(ADD);
  if (x > width - invNormZ * width / 6
   || x < invNormZ * width / 6) {
     
    fill(random(64, 192), random(192, 255), random(32, 64), random(64, 0.25 * z));
    noStroke();
    rect(0, 0, width, height);
    dx = -dx;
    x += 2 * dx;
  }
  if (y > height - invNormZ * height / 4
   || y < invNormZ * height / 8) {
     
    fill(random(64, 192), random(192, 255), random(32, 64), random(64, 0.25 * z));
    noStroke();
    rect(0, 0, width, height);
    dy = -dy;
    y += 2 * dy;
  }
  if (z > maxZ
   || z < 0) {
    
    fill(random(64, 192), random(192, 255), random(32, 64), random(dz > 0 ? 32 : 64, dz > 0 ? 64 : 128));
    noStroke();
    rect(0, 0, width, height);
    
    dz = -dz;
    z += 2 * dz;
  }
  
  blendMode(BLEND);
  fill(0, 64);
  noStroke();
  rect(random(-width * 0.5, width * 1.5),
       random(-height * 0.5, height * 1.5),
       width,
       height);

  blendMode(ADD);
  fill(128 + 128 * invNormZ,
       255,
       128 * invNormZ,
       32 + 128 * invNormZ);
  stroke(128, 8 + invNormZ * 16);
  strokeWeight(random(200 * normZ) + 5);
  ellipse(x,
          y,
          (height / 2) * pow(normZ, random(1.25 - normZ, 1.5 + invNormZ * 0.5)),
          (height / 2) * pow(normZ, random(1.25 - normZ, 1.5 + invNormZ * 0.5)));
  
  noFill();
  stroke(192, 255, 128, 16 * (maxZ - z) / maxZ);
  ellipse(x + random(-z / 4, z / 4),
          y + random(-z / 4, z / 4),
          height / 2 * normZ,
          height / 2 * normZ);
}